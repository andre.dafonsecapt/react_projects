import _ from "lodash";
import React from "react";
import StreamForm from "./StreamForm";
import { connect } from "react-redux";
import { fetchStream, editStream } from "../../actions";
import axios from "axios";

class StreamEdit extends React.Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
  }

  onSubmit = async (formValues) => {
    this.props.editStream(this.props.match.params.id, formValues);
  };

  /*   runScripts = () =>{

    const getResponseApi = axios.create({
      baseURL: "http://localhost:80/v3",
      headers: {
        "X-Auth-Token": "api-key mr478kb8ecpfosl8aku35wwxbr9rtrld",
      },
    });

    const campaingLists = await this.getCampaignLists(getResponseApi);
    console.log(campaingLists);

    const campaingList = await this.getCampaignList(getResponseApi, "Kmakr");
    console.log(campaingList);

    this.addEmailToList(getResponseApi, "dpppp@hotmail.com", "Kmakr");
  } */

  /*   getCampaignLists = async (getResponseApi) => {
    const campaingLists = await getResponseApi.get("/campaigns");

    return campaingLists.data;
  };

  getCampaignList = async (getResponseApi, campaignId) => {
    const campaingList = await getResponseApi.get(`/campaigns/${campaignId}`);

    return campaingList.data;
  };

  addEmailToList = async (getResponseApi, emailToAdd, campaignId) => {
    const response = await getResponseApi.post("/contacts", {
      email: emailToAdd,
      campaign: {
        campaignId: campaignId,
      },
    });
    console.log(response);
  }; */

  render() {
    if (!this.props.stream) {
      return <div>loading...</div>;
    }

    return (
      <div>
        <h3>Edit a Stream</h3>
        <StreamForm
          initialValues={_.pick(this.props.stream, "title", "description")}
          onSubmit={this.onSubmit}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id],
  };
};

export default connect(mapStateToProps, { fetchStream, editStream })(
  StreamEdit
);
