var cors = require("cors");
const express = require("express");
var app = express();
var proxy = require("express-http-proxy");

app.use(cors());

app.use("/", proxy("https://api.getresponse.com/v3"));

app.listen(80, function () {
  console.log("CORS-enabled web server listening on port 80");
});
