import axios from "axios";

export default axios.create({
  baseURL: "https://api.unsplash.com",
  headers: {
    Authorization: "Client-ID R71bPT-5B8kZRuF96tFSA6SBtZB6lHF9RGyitQs4HmQ",
  },
});
