import React from "react";

const About = () => {
  return (
    <div className="container">
      <h4 className="center">About</h4>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis
        labore tenetur natus! Reiciendis architecto maiores veritatis officia
        asperiores? Culpa ipsum aperiam corrupti amet consequatur sit aliquid id
        voluptatum deserunt cupiditate.
      </p>
    </div>
  );
};

export default About;
